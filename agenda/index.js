import http from 'http'
import express from 'express'
const app = express();
app.use(express.json())

let persons = [
  {
    id: 1,
    name: "Arto Hellas",
    number: "040-123456",
  },
  {
    id: 2,
    name: "Ada Lovelace",
    number: "39-44-5323523",
  },
  {
    id: 3,
    name: "Dan Abramov",
    number: "12-43-234345",
  },
  {
    id: 4,
    name: "Mary Poppendick",
    number: "39-23-6423122",
  },
];

app.get("/", (request, response) => {
  response.send("<h1>Hello World!</h1>");
});

app.get("/api/persons", (request, response) => {
  response.json(persons);
});

app.get("/info", (request, response) => {
  var cantidad = persons.length;
  const fecha = new Date();
  fecha.toDateString()

  response.send("<p>Phone has info for " + cantidad + " people</p> <p>" + fecha + "</p>" );
});

app.get("/api/persons/:id", (request, response) => {
  let idperson = request.params.id
  let person = persons.find(pers => pers.id == idperson)

  if(person) {
    response.json(person)
  } else {
    response.status(404).end("No se puedo encontrar su busqueda")
  }
});

app.delete("/api/persons/delete/:id", (request, response) => {
  const id = Number(request.params.id)
  let personsInit = [...persons]
  persons = persons.filter(person => person.id !== id)
  console.log(persons)
  if (persons.length !== personsInit.length) {
    response.status(202).end("Se elimino el recurso ficticiamente")
  } else {
    response.status(404).end("Persona a eliminar no encontrada")
  }
});

app.post("/api/persons", (request, response) => {
  let persona = request.body
  let personExist = persons.find(person => person.name === persona.name)
  if (persona.name==="" || persona.number==="") {
    response.status(406).json({"Error":"El campo name o number no tienen datos"})

  } else if (personExist) {
    response.status(406).json({"Error":"El nombre ya existe"})
    
  } else {
    var num = Math.floor((Math.random() * (100000-4))+4);
    let newPerson = {
      id: num,
      name: persona.name,
      number: persona.number,
    }
    persons.push(newPerson)
    response.status(201).end("Se agrego el nuevo dato")
    console.log(persons)
  }
});


const PORT = 3001;
app.listen(PORT, () => {
  console.log(`Server running on port ${PORT}`);
});
